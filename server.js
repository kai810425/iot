import Express from 'express';
import {WebSocketServer} from "ws";
import mqtt from 'mqtt';
const mqttServer = 'mqtt://mqtt-dashboard.com';
const subscribeTopic = 'nutc20231202test/';
const mqttClient = mqtt.connect(mqttServer);

let wss;

const app = Express();

const sendToBrowser = msg => {
    wss.clients.forEach(client => {
        client.send(msg);
    });
}

const sentToMqtt = (message) => {
    mqttClient.publish(subscribeTopic, message);
}

app.get('/', function (req, res) {
    return res.sendFile('/index.html', { root: '.' });
});

app.get('/on', function (req, res) {
    sentToMqtt('on');

    return res.end();
});

app.get('/off', function (req, res) {
    sentToMqtt('off');

    return res.end();
});

const server = app.listen(8080, () => {
    console.log("Started.");
});

wss = new WebSocketServer({ server });

wss.on('connection', ws => {
    //連結時執行此 console 提示
    console.log('瀏覽器連線');

    //當 WebSocket 的連線關閉時執行
    ws.on('close', () => {
        console.log('瀏覽器離線')
    })
});

mqttClient.on('connect', () => {
    mqttClient.subscribe(subscribeTopic, () => {
        console.log(`主題訂閱成功:${subscribeTopic}`);
    })
});

mqttClient.on('message', (topic, message) => {
    console.log({topic, message: message.toString()});
    sendToBrowser(message.toString());
})