import mqtt from 'mqtt';
const mqttServer = 'mqtt://mqtt-dashboard.com';
const subscribeTopic = 'nutc20231202test/';
const client = mqtt.connect(mqttServer);

client.on('connect', () => {
    client.subscribe(subscribeTopic, () => {
        console.log(`主題訂閱成功:${subscribeTopic}`);
        setInterval(() => {
            const d = new Date()
            client.publish(subscribeTopic, 'Hello mqtt - ' + d.toString());
        }, 1000);
    })
});

client.on('message', (topic, message) => {
    console.log({topic, message: message.toString()});
})