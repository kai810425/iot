import mqtt from 'mqtt';
import { SerialPort,ReadlineParser } from 'serialport';
const mqttServer = 'mqtt://mqtt-dashboard.com';
const subscribeTopic = 'nutc20231202test/';
const mqttClient = mqtt.connect(mqttServer);

console.log(await SerialPort.list());
const port = '/dev/tty.usbserial-14620';

const serial = new SerialPort({ path: port, baudRate: 9600 });

const sentToMqtt = (message) => {
    console.log(`sentToMqtt:${message}\n`);
    mqttClient.publish(subscribeTopic, message);
}

const receiveFromArduino = (message) => {
    console.log(`receiveFromArduino:${message}\n`);
    sentToMqtt(message);
}

const sentToArduino = (message) => {
    console.log(`sentToArduino:${message}\n`);
    serial.write( `${message}\n`);
}

const parser = new ReadlineParser();
serial.pipe(parser);
parser.on('data', receiveFromArduino);

mqttClient.on('connect', () => {
    mqttClient.subscribe(subscribeTopic, () => {
        console.log(`主題訂閱成功:${subscribeTopic}`);
    })
});

mqttClient.on('message', (topic, message) => {
    const stringMessage = message.toString();
    console.log({topic, message: stringMessage});
    if (["on","off"].includes(stringMessage)) {
        sentToArduino(stringMessage);
    }
})